package com.example.practice2;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.practice2.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button;
        EditText etext1, etext2;
        Spinner drop;
        TextView text3;


        button = findViewById(R.id.button);
        etext1 = findViewById(R.id.etext1);
        etext2 = findViewById(R.id.etext2);
        drop = findViewById(R.id.drop);
        text3 = findViewById(R.id.text3);

        String[] genders = {"Male","Female"};
        ArrayAdapter ad = new ArrayAdapter<String>(this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,genders);
        drop.setAdapter(ad);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etext1.getText().toString();
                String phone = etext2.getText().toString();
                String gender = drop.getSelectedItem().toString();

                text3.setText("Name: "+name+"\nGender: "+gender+"\nPhone number: "+phone);
            }
        });

    }
}